﻿module.exports = {
	'/guide/': require('../guide/sidebar'),

	'/mianshibaodian/java-low': require('../mianshibaodian/java-low/sidebar'),
	'/mianshibaodian/java-high': require('../mianshibaodian/java-high/sidebar'),
	'/mianshibaodian/juc-low': require('../mianshibaodian/juc-low/sidebar'),
	'/mianshibaodian/juc-center': require('../mianshibaodian/juc-center/sidebar'),
	'/mianshibaodian/juc-high': require('../mianshibaodian/juc-high/sidebar'),
	
	'/qdsystemproblem/qdstore': require('../qdsystemproblem/qdstore/sidebar'),
	'/qdsystemproblem/qdhospital': require('../qdsystemproblem/qdhospital/sidebar'),
}	
