module.exports = {
  title: '史上最菜大学生QDのBLOG',
  description: '人最宝贵的东西是生命。生命对人来说只有一次。因此，人的一生应当这样度过：当一个人回首往事时，不因虚度年华而悔恨，也不因碌碌无为而羞愧；这样，在他临死的时候，能够说，我把整个生命和全部精力都献给了人生最宝贵的事业——为人类的解放而奋斗 --- 奥斯特洛夫斯基',
  dest: './dist',
  host:'localhost',
  port: '1314',
  head: [
      ['link', {rel: 'icon', href: '/img/logo.ico'}],
      ["link", { rel: "stylesheet", href: "/css/style.css" }],
	    ["script", { charset: "utf-8", src: "/js/main.js" }],
      ["link", { rel: "stylesheet", href: "https://cdn.jsdelivr.net/npm/katex@0.11.1/dist/katex.min.css" }],
      ['meta', {name: 'referrer', content: 'no-referrer-when-downgrade'}],
      // 移动栏优化
      ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no' }],
      // 引入jquery
      ["script", {
        "language": "javascript",
        "type": "text/javascript",
        "src": "https://cdn.bootcdn.net/ajax/libs/jquery/3.5.1/jquery.min.js"
      }],
      // 引入鼠标点击脚本
      ["script", {
        "language": "javascript",
        "type": "text/javascript",
        "src": "/js/MouseClickEffect.js"
      }]
    ],
  markdown: {
      lineNumbers: true
  },
  // theme:'reco',  // 使用这个主题会导致我的侧边栏失效
    theme: 'vdoing',

  themeConfig: {
      yuu: {
        disableDarkTheme: true,
      },
      sidebar: 'auto',   // 自动生成侧边栏
      heroImage:'https://hqd-typora.oss-cn-shanghai.aliyuncs.com/logo.jpg',
      // sidebarDepth:6,
      logo:'https://hqd-typora.oss-cn-shanghai.aliyuncs.com/logo.jpg',
      nav: require("./nav.js"),
      sidebar: require("./sidebar.js"),
      sidebarDepth: 2,
      lastUpdated: '最近更新时间：',
      searchMaxSuggestoins: 10,
      serviceWorker: {
          updatePopup: {
              message: "有新的内容.",
              buttonText: '更新'
          }
      },
      editLinks: true,
      editLinkText: '在 GitHub 上编辑此页 ！'
  },
  plugins: [
      ['@vuepress/back-to-top'],
      // ['@vuepress/last-updated',true],
      ['@vuepress-reco/extract-code'],
	  // 线条带
      ['@vuepress/nprogress'],
      // 阅读进度条
      ['reading-progress'],
      // 全文搜索
      ['fulltext-search'],
      // ['autobar'],
	  // 从下往上滚动
      // ['go-top'],
      // 名人名言
      ["vuepress-plugin-boxx"],
      // 页面滚动时自动激活侧边栏链接
      ['@vuepress/active-header-links', {
        sidebarLinkSelector: '.sidebar-link',
        headerAnchorSelector: '.header-anchor'
      }],
	  // 代码复制
      // ['vuepress-plugin-code-copy', true],
      ["vuepress-plugin-nuggets-style-copy", {
        copyText: "复制代码",
        tip: {
            content: "复制成功!"
        }
      }],
      ['ribbon',{
           size: 90, // width of the ribbon, default: 90
           opacity: 0.8, // opacity of the ribbon, default: 0.3
           zIndex: -1, // z-index property of the background, default: -1
        }],
	  // 鼠标点击效果
     ['cursor-effects',{
         size: 2, // size of the particle, default: 2
         shape: ['star' | 'circle'], // shape of the particle, default: 'star'
         zIndex: 999999999, // z-index property of the canvas, default: 999999999
      }],
      ['@vuepress/pwa', {
        serviceWorker: true,
        updatePopup: true
      }],
	  // 图片缩放
      ['@vuepress/medium-zoom', {
        selector: 'img.zoom-custom-imgs',
        options: {
          margin: 16
        }
      }],
      ['@vuepress/active-header-links',{
        sidebarLinkSelector: '.sidebar-link',
        headerAnchorSelector: '.header-anchor'
     }],
     ["dynamic-title",{
        showIcon: "/img/logo.ico",
        showText: "小主人你又来看我了！",
        hideIcon: "/img/logo.ico",
        hideText: "小主人你怎么走了！",
        recoverTime: 2000
      }],
      ['@vuepress-reco/vuepress-plugin-bulletin-popover', {
        width: '300px', // 默认 260px
        title: '消息提示',
        body: [
          {
            type: 'title',
            content: '欢迎加微信交流 🎉🎉🎉',
            style: 'text-aligin: center;'
          },
          {
            type: 'image',
            src: './img/wx.jpg'
          }
        ],
        footer: [
          {
            type: 'button',
            text: '打赏',
            link: './img/wxpay.png'
          }
        ]
      }],
      ['dline',{
          zIndex: -1, // 背景层级
          opacity: .9, // 背景透明度
          color: '#3EAF7C', // 线条颜色 16进制
          count: 166 // 线条密度
        }],
          ['@vuepress/last-updated',{
            transformer: (timestamp, lang) => {
              // 不要忘了安装 moment
              const moment = require('moment')
              moment.locale(lang)
              return moment(timestamp).fromNow()
            }
          }],
          ['@vuepress/active-header-links',{
            sidebarLinkSelector: '.sidebar-link',
            headerAnchorSelector: '.header-anchor'
        }],
        // 配置插件vuepress-plugin-thirdparty-search, 默认主题的搜索框集成第三方搜索引擎
        ["thirdparty-search",{
            thirdparty: [
              // 可选，默认 []
              {
                title: "在百度中搜索", // 在搜索结果显示的文字
                frontUrl: "https://www.baidu.com/s?wd=", // 搜索链接的前面部分
                behindUrl: "" // 搜索链接的后面部分，可选，默认 ''
              },
              {
                title: "在必应中搜索",
                frontUrl: "https://cn.bing.com/search?q="
              }
            ]
          }],
          ['robots',{
            host: "http://www.example.com",
            disallowAll: true,
            sitemap: "/assets/xml/sitemap.xml",
        }],

    ]
}
