﻿module.exports = [
    {
        text: '关于作者', link: 'https://pic.imgdb.cn/item/6233977c5baa1a80aba4034a.jpg'
    },
    {
        text: '太优秀指南', link: '/guide/'
    },
    {
        text: '项目问题',
		items: [
            {text: 'QDSTORE', link: '/qdsystemproblem/qdstore/'},
			{text: 'QDHOSPITAL', link: '/qdsystemproblem/qdhospital/'},
            {text: 'QDMALL', link: '/qdsystemproblem/qdmall/'},
			{text: 'QDEDU', link: '/qdsystemproblem/qdedu/'},
			{text: 'QDLOAN', link: '/qdsystemproblem/qdloan/'},
			{text: 'QDWIKI', link: '/qdsystemproblem/qdwiki/'},
        ]
    },
    {
        text: 'Java',
		items: [
            {text: 'Java基础', link: '/mianshibaodian/java-low/'},
            {text: 'Java高级', link: '/mianshibaodian/java-high/'},
			{text: 'JUC(一)', link: '/mianshibaodian/juc-low/'},
			{text: 'JUC(二)', link: '/mianshibaodian/juc-center/'},
			{text: 'JUC(三)', link: '/mianshibaodian/juc-high/'},
        ]
    },
    {
        text: 'MySQL',
		items: [
            {text: 'Java基础', link: '/mianshibaodian/java-low/'},
            {text: 'Java高级', link: '/mianshibaodian/java-high/'},
			{text: 'JUC(一)', link: '/mianshibaodian/juc-low/'},
			{text: 'JUC(二)', link: '/mianshibaodian/juc-center/'},
			{text: 'JUC(三)', link: '/mianshibaodian/juc-high/'},
        ]
    },
    {
        text: '面试宝典',
		items: [
            {text: 'Java基础', link: '/mianshibaodian/java-low/'},
            {text: 'Java高级', link: '/mianshibaodian/java-high/'},
			{text: 'JUC(一)', link: '/mianshibaodian/juc-low/'},
			{text: 'JUC(二)', link: '/mianshibaodian/juc-center/'},
			{text: 'JUC(三)', link: '/mianshibaodian/juc-high/'},
        ]
    },
    // {
    //     text: 'Languages',
    //     items: [
    //       { text: 'Chinese', link: '/language/chinese' },
    //       { text: 'English', link: '/language/english' }
    //     ]
    // },
    {
        text: '工具箱',
        items: [
			{
                text: '在线编辑',
				items: [
					{text: '图片压缩', link: 'https://tinypng.com/'}
				]
            },
			{
                text: '在线服务',
				items: [
					{text: '阿里云', link: 'https://www.aliyun.com/'},
					{text: '腾讯云', link: 'https://cloud.tencent.com/'}
				]
            },
			{
                text: '博客指南',
				items: [
					{text: '掘金', link: 'https://juejin.im/'},
					{text: 'CSDN', link: 'https://blog.csdn.net/'}
				]
            }
        ]
    }
]
