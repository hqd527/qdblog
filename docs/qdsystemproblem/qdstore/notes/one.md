---
title: one
date: 2022-08-24 09:59:28
permalink: /pages/08239e/
categories:
  - qdsystemproblem
  - qdstore
  - notes
tags:
  - 
---
# 项目BUG汇总

## 【1】搭建renren-fast项目报错

搭建人人快速开发平台时，在数据库连接部分由于我使用的是自己服务器中的数据库，如果使用的不是本地localhost的话必须在url中加上`&useSSL=false`否则会出现如下错误：

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220315232041157.png)

解决方法就是在url中加上`&useSSL=false`,重启服务可以发现问题顺利解决。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220315232404691.png)

当然了，如果使用的是localhost本地数据库的话，`&useSSL=false`可加可不加。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220315231622206.png)



> 问题出现原因：
>
> web应用中连接mysql数据库时后台会出现这样的提示：
>
> Establishing SSL connection without server’s identity verification is not recommended. According to MySQL 5.5.45+,
> 5.6.26+ and 5.7.6+ requirements SSL connection must be established by default if explicit option isn’t set. For compliance with existing applications not using SSL the verifyServerCertificate property is set to ‘false’. You need either to explicitly disable SSL by setting useSSL=false, or set useSSL=true and provide truststore for server certificate verification.
>
> 翻译：
>
> 不建议在没有服务器身份验证的情况下建立SSL连接。根据MySQL 5.5.45+，5.6.26+和5.7.6+要求如果未设置显式选项，默认情况下必须建立SSL连接。为了符合不使用SSL的现有应用程序，verifyServerCertificate属性设置为“false”。您需要通过设置useSSL=false显式禁用SSL，或者设置useSSL=true并为服务器证书验证提供信任库。
>
> 原因是MySQL在高版本需要指明是否进行SSL连接。
>
>   ```properties
>   SSL协议提供服务主要： 		
>          1）认证用户服务器，确保数据发送到正确的服务器； 　　 .
>          2）加密数据，防止数据传输途中被窃取使用；
>          3）维护数据完整性，验证数据在传输过程中是否丢失；
>   
>      当前支持SSL协议两层：
>      	 	SSL记录协议（SSL Record Protocol）：建立靠传输协议（TCP）高层协议提供数据封装、压缩、加密等基本功能支持
>   	    SSL握手协议（SSL Handshake Protocol）：建立SSL记录协议用于实际数据传输始前通讯双进行身份认证、协商加密
>   	    算法、 交换加密密钥等。
>   	    
>   解决方案如下：
>   在mysql连接字符串url中加入ssl=true或者false即可，如下所示。
>   url=jdbc:mysql://127.0.0.1:3306/framework?useSSL=false
>   ```



## 【2】安装启动运行renren-fast-vue项目报错

在首次运行renren-fast-vue后台管理系统项目时，使用`npm install`报如下错误。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317000219964.png)

解决办法：先删除由于报错所安装的node_modules文件夹和package-lock.json文件。然后使用`管理员权限运行cmd`。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317000142167.png)

执行`npm install --global --production windows-build-tools`命令，显示安装成功，如下图所示。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317000007142.png)

再次执行`npm install`安装好node_modules和package-lock.json文件；最后执行`npm run dev`即可正常运行启动项目。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317001304018.png)



## 【3】关于Maven创建module弹出被忽略(Ignored)的Ignored pom.xml文件如何解决

由于在第一次将renren-generator作为一个maven工程放进我的聚合项目中时不满意，于是我进行过一次将renren-fast项目删除，随后又一次引入到聚合项目中，最终导致出现如下问题：`Ignored pom.xml`，也就是pom文件不生效。

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317190940272.png)

问题分析：

**由于相同名称的module在之前被创建过，因此在IDEA中留有痕迹。重新创建一个新的同名module会让IDEA误以为是之前被删除掉的module，所以才自动将这个pom.xml文件忽略了呢。**

解决方案：

点击file--->setting

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317191425793.png)

找到maven下的Ignored Files，取消勾选对应的pom.xml文件即可！

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317191537880.png)

最后，重现刷新一遍maven，问题顺利解决！

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220317191757682.png)



## 【4】yml配置文件无法导出问题（也就是没编译）

启动服务出现如下问题：

application.yml配置文件是生效的，也就是被识别的

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318003004243.png)

但是依然报错，所以感觉很奇怪呀！

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318003056960.png)

这时，我使用了最笨的办法：将application.yml复制到target目录下

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318003249803.png)

随后重新启动服务，发现运行成功！

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318003350091.png)

那么从这里很明显可以得出结论：那就是yml文件压根没有被	`编译`！！！

因此我联想到以前遇到过xml资源文件没有导出的情况，也给yml文件做了导出配置，具体操作如下：

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318003805508.png)

加上这段配置后，服务也是可以顺利启动了！【具体原因暂时不知，大佬说他也遇到过这个问题，他是包的依赖冲突了。遗憾的是我暂时还不会排查！！】

或者：也可以这么解决：

点击file ---> project structure

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318200953713.png)

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318201203223.png)



## 【5】整合Mybatis-Plus时，添加`@MapperScan("com.hqd527.qdstore.coupon.dao")`注解时粗心报错，写成了`@MapperScan("com.hqd527.qdstore.coupon")`导致如下错误：

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318202159540.png)

```shell
org.springframework.context.annotation.ConflictingBeanDefinitionException: Annotation-specified bean name 'couponHistoryService' for bean class [com.hqd527.qdstore.coupon.service.CouponHistoryService] conflicts with existing, non-compatible bean definition of same name and class [com.hqd527.qdstore.coupon.service.impl.CouponHistoryServiceImpl]
	at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.checkCandidate(ClassPathBeanDefinitionScanner.java:349) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.mybatis.spring.mapper.ClassPathMapperScanner.checkCandidate(ClassPathMapperScanner.java:256) ~[mybatis-spring-2.0.5.jar:2.0.5]
	at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.doScan(ClassPathBeanDefinitionScanner.java:287) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.mybatis.spring.mapper.ClassPathMapperScanner.doScan(ClassPathMapperScanner.java:181) ~[mybatis-spring-2.0.5.jar:2.0.5]
	at org.springframework.context.annotation.ClassPathBeanDefinitionScanner.scan(ClassPathBeanDefinitionScanner.java:254) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.mybatis.spring.mapper.MapperScannerConfigurer.postProcessBeanDefinitionRegistry(MapperScannerConfigurer.java:356) ~[mybatis-spring-2.0.5.jar:2.0.5]
	at org.springframework.context.support.PostProcessorRegistrationDelegate.invokeBeanDefinitionRegistryPostProcessors(PostProcessorRegistrationDelegate.java:275) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.springframework.context.support.PostProcessorRegistrationDelegate.invokeBeanFactoryPostProcessors(PostProcessorRegistrationDelegate.java:125) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.springframework.context.support.AbstractApplicationContext.invokeBeanFactoryPostProcessors(AbstractApplicationContext.java:706) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.springframework.context.support.AbstractApplicationContext.refresh(AbstractApplicationContext.java:532) ~[spring-context-5.2.1.RELEASE.jar:5.2.1.RELEASE]
	at org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext.refresh(ServletWebServerApplicationContext.java:141) ~[spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at org.springframework.boot.SpringApplication.refresh(SpringApplication.java:747) [spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at org.springframework.boot.SpringApplication.refreshContext(SpringApplication.java:397) [spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:315) [spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1226) [spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at org.springframework.boot.SpringApplication.run(SpringApplication.java:1215) [spring-boot-2.2.1.RELEASE.jar:2.2.1.RELEASE]
	at com.hqd527.qdstore.coupon.CouponQDSTOREApplication.main(CouponQDSTOREApplication.java:17) [classes/:na]
```

解决方案：写的应该是mapper层的全路径`@MapperScan("com.hqd527.qdstore.coupon.dao")`,遍可顺利解决！

![](https://hqd-typora.oss-cn-shanghai.aliyuncs.com/image-20220318202546673.png)
