---
title: one
date: 2022-08-24 09:59:28
permalink: /pages/c8fff3/
categories:
  - qdsystemproblem
  - qdhospital
  - notes
tags:
  - 
---
# 项目BUG汇总

## 【1】在项目中的service-util模块中引入自己写的RedisConfig配置类，但是发现有bean没有被注入

![image-20220329191811989](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220329191811989.png)

#### 出现问题原因：我们的service-hosp的启动类和service-util的RedisConfig.java不在同一个包中，导致配置类没办法自动被扫描到。

![image-20220329192247900](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220329192247900.png)

![image-20220329192330937](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220329192330937.png)

#### 解决方案：在service-hosp模块的启动类上加上`@ComponentScan(basePackages = "com.hqd527.qdhospital")`

![image-20220329192412147](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220329192412147.png)

#### 最后，最好刷新一下maven，然后再次点进RedisConfig.java文件中可以看到问题顺利解决。

![image-20220329192555875](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220329192555875.png)



## 【2】组件重用问题

#### 出现问题：如果我们先点击了编辑按钮，再点击添加导航，会发现数据依然没有清空，还是处于回显状态

![image-20220331234603230](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220331234603230.png)

![image-20220331234859258](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220331234859258.png)

----

#### 分析原因：vue-router导航切换 时，如果两个路由都渲染同个组件，组件的生命周期方法（created或者mounted）不会再被调用, 组件会被重用，显示上一个路由渲染出来的数据

-----

#### 解决方案：可以简单的在 router-view上加上一个唯一的key，来保证路由切换时都会重新触发生命周期方法，确保组件被重新初始化。修改 `src/views/layout/components/AppMain.vue` 文件如下：

![image-20220331235406866](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/image-20220331235406866.png)