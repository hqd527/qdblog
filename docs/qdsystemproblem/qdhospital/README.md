---
title: README
date: 2022-08-24 09:59:28
permalink: /pages/7a2159/
categories:
  - qdsystemproblem
  - qdhospital
tags:
  - 
---
### 启迪协和医院架构图

![尚医通架构图](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/尚医通架构图.png)



### 启迪协和医院业务流程

![尚医通业务流程](http://hqd-typora.oss-cn-shanghai.aliyuncs.com/img/尚医通业务流程.png)