﻿module.exports = [
		{
		  title:'swagger配置以及注解',
		  collapsable: true,
		  children:[
			'/guide/notes/swagger',
		  ]
		},
		{
		  title:'Mybatis',
		  collapsable: true,
		  children:[
			'/guide/notes/mybatis',
		  ]
		},
		{
		  title:'MyBatisPlus',
		  collapsable: true,
		  children:[
			'/guide/notes/mybatis-plus',
		  ]
		},
		{
		  title:'谷粒商城基础篇',
		  collapsable: true,
		  children:[
			'/guide/notes/谷粒商城基础篇',
		  ]
		}
	]
