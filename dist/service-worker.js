/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "863d7821616112420bf258ba2e2204d6"
  },
  {
    "url": "assets/css/0.styles.ea863f3c.css",
    "revision": "55812afc8491e6ceff9d0881b5d457dc"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/js/10.37c0f1d6.js",
    "revision": "1220a121edf996a35120fd9708459901"
  },
  {
    "url": "assets/js/11.48aa5aec.js",
    "revision": "5eecf42f1c8e89e5005e315999078677"
  },
  {
    "url": "assets/js/12.fff5859c.js",
    "revision": "d2ed6464241f9fa80c1397a81855f584"
  },
  {
    "url": "assets/js/13.f59e1377.js",
    "revision": "496015fcde8f3863a33ffd3b0f828622"
  },
  {
    "url": "assets/js/14.04559151.js",
    "revision": "0885f4d33601622764f809cf78b39066"
  },
  {
    "url": "assets/js/15.20e97074.js",
    "revision": "17e548ede4551cc7706f22a7b965eb3e"
  },
  {
    "url": "assets/js/16.5300548c.js",
    "revision": "9dcf7ac4b3f759cf8d5a4b8cb5e50ec4"
  },
  {
    "url": "assets/js/17.dbffb62e.js",
    "revision": "80b4a73ad4844b32763c9c0611a1e6f5"
  },
  {
    "url": "assets/js/18.1ae4127b.js",
    "revision": "1d8302fa78148791aa0963d9d69a2274"
  },
  {
    "url": "assets/js/19.229a732a.js",
    "revision": "3250a3bd1a08376e6ca7550e9d62cef0"
  },
  {
    "url": "assets/js/2.924116a4.js",
    "revision": "b8fde13a2a670cb2153150bad36f27fe"
  },
  {
    "url": "assets/js/20.a5ef3101.js",
    "revision": "6dc677da6ade60bb7f1fa6c768028d9f"
  },
  {
    "url": "assets/js/21.e70264fe.js",
    "revision": "4e79f48c52676ca3612e54ed8c16015b"
  },
  {
    "url": "assets/js/22.711f4756.js",
    "revision": "ed5862fca38ef88961a78b2a0a3a6ba0"
  },
  {
    "url": "assets/js/23.ad17cb46.js",
    "revision": "4ca4f6063583bd87f300a3e63bea615d"
  },
  {
    "url": "assets/js/24.bc4b1ea6.js",
    "revision": "042b1ba596fb07ca57acaefb7a6043fb"
  },
  {
    "url": "assets/js/25.c440610f.js",
    "revision": "a3da9a5ed0a45e3240ccdbd8a79f0e93"
  },
  {
    "url": "assets/js/26.5594caca.js",
    "revision": "7940ec3f26cf0de22f6cc3bff1e8e8e6"
  },
  {
    "url": "assets/js/27.5a7d6104.js",
    "revision": "725af54d3d0fe00270eaf303795f3730"
  },
  {
    "url": "assets/js/28.ac6217b6.js",
    "revision": "ab3e25790e5590f12e511b9ddfed0f6e"
  },
  {
    "url": "assets/js/29.fd94b5a8.js",
    "revision": "7f624534af9461522995893272692ae6"
  },
  {
    "url": "assets/js/3.34abe1ee.js",
    "revision": "c27b2c978d98641d060a7c14e25836a0"
  },
  {
    "url": "assets/js/30.3f31cdf6.js",
    "revision": "393aa5b7555115fa2416937eb8e6ac24"
  },
  {
    "url": "assets/js/31.0acaf59c.js",
    "revision": "178393f82edb92abbf7be390bf1218c5"
  },
  {
    "url": "assets/js/32.6eb18ed4.js",
    "revision": "b7dde8beb06f6935c0f47c4116f0e749"
  },
  {
    "url": "assets/js/4.631b9c9d.js",
    "revision": "ee617fdf700f9cf5b632340a00d39d1f"
  },
  {
    "url": "assets/js/5.0b5ed0bc.js",
    "revision": "e21e7d79ef7ef9f3cac03df2aff116be"
  },
  {
    "url": "assets/js/6.893d7a5c.js",
    "revision": "d4d10005d39f5c98916ae77d30b4a1e2"
  },
  {
    "url": "assets/js/7.c19a622e.js",
    "revision": "d601cf94cb1d27708ffa58ce4380c7d3"
  },
  {
    "url": "assets/js/8.d11386da.js",
    "revision": "ef8e6cd7d16c88bf4e72cac84d7b1ba5"
  },
  {
    "url": "assets/js/9.3d9150b1.js",
    "revision": "370acd1ffdd37f6011963e03c0ac317a"
  },
  {
    "url": "assets/js/app.70e96b21.js",
    "revision": "efbbbe7c5ed29ce6b87282b5ba358781"
  },
  {
    "url": "css/style.css",
    "revision": "b0f69cbf649bd2e2b309f50789c3c4c1"
  },
  {
    "url": "guide/index.html",
    "revision": "87b002fcf4885528ae0902f63e0e6fd2"
  },
  {
    "url": "guide/notes/mybatis-plus.html",
    "revision": "3ba634810888e0bddd685645c2825b5e"
  },
  {
    "url": "guide/notes/mybatis.html",
    "revision": "5d28b7af849f5cb4818868ce74c37352"
  },
  {
    "url": "guide/notes/Spring Cloud Alibaba.html",
    "revision": "f4407218fa8c80519b84c89d4d1fc971"
  },
  {
    "url": "guide/notes/SpringCloud 微服务工具集.html",
    "revision": "5fb93d54d462d9d03e98ae4cc45d95ae"
  },
  {
    "url": "guide/notes/swagger.html",
    "revision": "b48c24a0cccf4ba9037c5e05671be89b"
  },
  {
    "url": "guide/notes/谷粒商城基础篇.html",
    "revision": "de352386e67a1b1388bf22d961df99f0"
  },
  {
    "url": "img/main.jpg",
    "revision": "a487357da7bc874eeb98116f87c3a516"
  },
  {
    "url": "img/sidephoto.jpg",
    "revision": "2ea4fd70bfb15e4a13688be47f2f2c03"
  },
  {
    "url": "img/wx.jpg",
    "revision": "49c70c0c6e0eeae93fe657cf293b5488"
  },
  {
    "url": "img/wxpay.png",
    "revision": "4a86a4fe130125e96b9fa2579154b123"
  },
  {
    "url": "index.html",
    "revision": "6a6892d2c0483d525a7c77dd3462a10f"
  },
  {
    "url": "js/main.js",
    "revision": "bfb38294cd0f42f7ccb7be5ede75f5f3"
  },
  {
    "url": "js/MouseClickEffect.js",
    "revision": "c8209619f9a8260aee66ce7691c9f2ad"
  },
  {
    "url": "mianshibaodian/index.html",
    "revision": "492f07154cd4d144f90e2c8be1018d1f"
  },
  {
    "url": "mianshibaodian/java-high/index.html",
    "revision": "e935223b1fea74c7e96e4d39f2262cb2"
  },
  {
    "url": "mianshibaodian/java-high/notes/java-02.html",
    "revision": "83a0a0ab7cce19e8f632c3012a66ca7d"
  },
  {
    "url": "mianshibaodian/java-low/index.html",
    "revision": "48e8b85045846d8daa56867e46df4836"
  },
  {
    "url": "mianshibaodian/java-low/notes/java-01.html",
    "revision": "2c10869fda4b729c9c58b847c984c4ac"
  },
  {
    "url": "mianshibaodian/juc-center/index.html",
    "revision": "7db292f106d9cddef4693e9223a02ce5"
  },
  {
    "url": "mianshibaodian/juc-center/notes/juc-02.html",
    "revision": "37c2ceceb172bd1064d037451f9497d0"
  },
  {
    "url": "mianshibaodian/juc-high/index.html",
    "revision": "1ff5c60b4dae0053f43f38fc797a08a1"
  },
  {
    "url": "mianshibaodian/juc-high/notes/juc-03.html",
    "revision": "50577833082e495353e0ff57b5d10df9"
  },
  {
    "url": "mianshibaodian/juc-low/index.html",
    "revision": "3082a1d0dde56d610e451e197a56b3c5"
  },
  {
    "url": "mianshibaodian/juc-low/notes/juc-01.html",
    "revision": "b16c841b0bfdaa729d9baf42e515c347"
  },
  {
    "url": "qdsystemproblem/index.html",
    "revision": "a4aad5977ea0495abc6e2c583e1bcd43"
  },
  {
    "url": "qdsystemproblem/qdhospital/index.html",
    "revision": "ae4e3c71f50a09f0f7f3a79b6d12b610"
  },
  {
    "url": "qdsystemproblem/qdhospital/notes/one.html",
    "revision": "ebf7e16d02bc6f5fa3809c2473b418d2"
  },
  {
    "url": "qdsystemproblem/qdstore/index.html",
    "revision": "c44043807162c4bacb17378924ef74d6"
  },
  {
    "url": "qdsystemproblem/qdstore/notes/one.html",
    "revision": "4c83bc1d9ac3a99e14f8e9f6cb145d58"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
