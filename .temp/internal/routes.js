/**
 * Generated by "@vuepress/internal-routes"
 */

import { injectComponentOption, ensureAsyncComponentsLoaded } from '@app/util'
import rootMixins from '@internal/root-mixins'
import GlobalLayout from "D:\\QDSystem\\qdblog\\node_modules\\@vuepress\\core\\lib\\client\\components\\GlobalLayout.vue"

injectComponentOption(GlobalLayout, 'mixins', rootMixins)
export const routes = [
  {
    name: "v-b100d268",
    path: "/archives/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-b100d268").then(next)
    },
  },
  {
    path: "/archives/index.html",
    redirect: "/archives/"
  },
  {
    path: "/@pages/archivesPage.html",
    redirect: "/archives/"
  },
  {
    name: "v-eb679968",
    path: "/tags/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-eb679968").then(next)
    },
  },
  {
    path: "/tags/index.html",
    redirect: "/tags/"
  },
  {
    path: "/@pages/tagsPage.html",
    redirect: "/tags/"
  },
  {
    name: "v-4316d122",
    path: "/pages/0f543b/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-4316d122").then(next)
    },
  },
  {
    path: "/pages/0f543b/index.html",
    redirect: "/pages/0f543b/"
  },
  {
    path: "/guide/",
    redirect: "/pages/0f543b/"
  },
  {
    name: "v-c64215b2",
    path: "/pages/f55a93/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-c64215b2").then(next)
    },
  },
  {
    path: "/pages/f55a93/index.html",
    redirect: "/pages/f55a93/"
  },
  {
    path: "/guide/notes/mybatis-plus.html",
    redirect: "/pages/f55a93/"
  },
  {
    name: "v-ed02d2a8",
    path: "/categories/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-ed02d2a8").then(next)
    },
  },
  {
    path: "/categories/index.html",
    redirect: "/categories/"
  },
  {
    path: "/@pages/categoriesPage.html",
    redirect: "/categories/"
  },
  {
    name: "v-07cdcea5",
    path: "/pages/07daf2/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-07cdcea5").then(next)
    },
  },
  {
    path: "/pages/07daf2/index.html",
    redirect: "/pages/07daf2/"
  },
  {
    path: "/guide/notes/mybatis.html",
    redirect: "/pages/07daf2/"
  },
  {
    name: "v-47e73ef6",
    path: "/pages/75d29b/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-47e73ef6").then(next)
    },
  },
  {
    path: "/pages/75d29b/index.html",
    redirect: "/pages/75d29b/"
  },
  {
    path: "/guide/notes/swagger.html",
    redirect: "/pages/75d29b/"
  },
  {
    name: "v-20c618d2",
    path: "/pages/5e64ec/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-20c618d2").then(next)
    },
  },
  {
    path: "/pages/5e64ec/index.html",
    redirect: "/pages/5e64ec/"
  },
  {
    path: "/mianshibaodian/java-high/",
    redirect: "/pages/5e64ec/"
  },
  {
    name: "v-10bce0df",
    path: "/pages/928690/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-10bce0df").then(next)
    },
  },
  {
    path: "/pages/928690/index.html",
    redirect: "/pages/928690/"
  },
  {
    path: "/mianshibaodian/",
    redirect: "/pages/928690/"
  },
  {
    name: "v-5dedeb09",
    path: "/pages/8e3575/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-5dedeb09").then(next)
    },
  },
  {
    path: "/pages/8e3575/index.html",
    redirect: "/pages/8e3575/"
  },
  {
    path: "/guide/notes/谷粒商城基础篇.html",
    redirect: "/pages/8e3575/"
  },
  {
    name: "v-850a81b0",
    path: "/pages/e331c7/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-850a81b0").then(next)
    },
  },
  {
    path: "/pages/e331c7/index.html",
    redirect: "/pages/e331c7/"
  },
  {
    path: "/mianshibaodian/java-high/notes/java-02.html",
    redirect: "/pages/e331c7/"
  },
  {
    name: "v-219aaddc",
    path: "/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-219aaddc").then(next)
    },
  },
  {
    path: "/index.html",
    redirect: "/"
  },
  {
    name: "v-37d454d8",
    path: "/pages/604167/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-37d454d8").then(next)
    },
  },
  {
    path: "/pages/604167/index.html",
    redirect: "/pages/604167/"
  },
  {
    path: "/mianshibaodian/java-low/",
    redirect: "/pages/604167/"
  },
  {
    name: "v-2acb8516",
    path: "/pages/67c177/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-2acb8516").then(next)
    },
  },
  {
    path: "/pages/67c177/index.html",
    redirect: "/pages/67c177/"
  },
  {
    path: "/mianshibaodian/juc-center/",
    redirect: "/pages/67c177/"
  },
  {
    name: "v-4715a27c",
    path: "/pages/c365b9/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-4715a27c").then(next)
    },
  },
  {
    path: "/pages/c365b9/index.html",
    redirect: "/pages/c365b9/"
  },
  {
    path: "/mianshibaodian/juc-high/",
    redirect: "/pages/c365b9/"
  },
  {
    name: "v-601de7dc",
    path: "/pages/f72e08/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-601de7dc").then(next)
    },
  },
  {
    path: "/pages/f72e08/index.html",
    redirect: "/pages/f72e08/"
  },
  {
    path: "/mianshibaodian/java-low/notes/java-01.html",
    redirect: "/pages/f72e08/"
  },
  {
    name: "v-20d6ffa6",
    path: "/pages/8670a7/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-20d6ffa6").then(next)
    },
  },
  {
    path: "/pages/8670a7/index.html",
    redirect: "/pages/8670a7/"
  },
  {
    path: "/qdsystemproblem/",
    redirect: "/pages/8670a7/"
  },
  {
    name: "v-4ec2ee52",
    path: "/pages/4fe6e5/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-4ec2ee52").then(next)
    },
  },
  {
    path: "/pages/4fe6e5/index.html",
    redirect: "/pages/4fe6e5/"
  },
  {
    path: "/mianshibaodian/juc-low/",
    redirect: "/pages/4fe6e5/"
  },
  {
    name: "v-d8ce621c",
    path: "/pages/dfd34d/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-d8ce621c").then(next)
    },
  },
  {
    path: "/pages/dfd34d/index.html",
    redirect: "/pages/dfd34d/"
  },
  {
    path: "/mianshibaodian/juc-high/notes/juc-03.html",
    redirect: "/pages/dfd34d/"
  },
  {
    name: "v-71e16c1a",
    path: "/pages/b4e034/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-71e16c1a").then(next)
    },
  },
  {
    path: "/pages/b4e034/index.html",
    redirect: "/pages/b4e034/"
  },
  {
    path: "/mianshibaodian/juc-center/notes/juc-02.html",
    redirect: "/pages/b4e034/"
  },
  {
    name: "v-d7f8bdf4",
    path: "/pages/883180/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-d7f8bdf4").then(next)
    },
  },
  {
    path: "/pages/883180/index.html",
    redirect: "/pages/883180/"
  },
  {
    path: "/mianshibaodian/juc-low/notes/juc-01.html",
    redirect: "/pages/883180/"
  },
  {
    name: "v-10f27c30",
    path: "/pages/c8fff3/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-10f27c30").then(next)
    },
  },
  {
    path: "/pages/c8fff3/index.html",
    redirect: "/pages/c8fff3/"
  },
  {
    path: "/qdsystemproblem/qdhospital/notes/one.html",
    redirect: "/pages/c8fff3/"
  },
  {
    name: "v-561ba35c",
    path: "/pages/d8bbfd/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-561ba35c").then(next)
    },
  },
  {
    path: "/pages/d8bbfd/index.html",
    redirect: "/pages/d8bbfd/"
  },
  {
    path: "/qdsystemproblem/qdstore/",
    redirect: "/pages/d8bbfd/"
  },
  {
    name: "v-24ef27f8",
    path: "/pages/08239e/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-24ef27f8").then(next)
    },
  },
  {
    path: "/pages/08239e/index.html",
    redirect: "/pages/08239e/"
  },
  {
    path: "/qdsystemproblem/qdstore/notes/one.html",
    redirect: "/pages/08239e/"
  },
  {
    name: "v-09e5598c",
    path: "/pages/7a2159/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-09e5598c").then(next)
    },
  },
  {
    path: "/pages/7a2159/index.html",
    redirect: "/pages/7a2159/"
  },
  {
    path: "/qdsystemproblem/qdhospital/",
    redirect: "/pages/7a2159/"
  },
  {
    path: '*',
    component: GlobalLayout
  }
]